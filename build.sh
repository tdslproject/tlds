#!/bin/bash -x

cd ..
cp -Rf tlds trans-dev
cd trans-dev
./bootstrap.sh
autoreconf
cd ../trans-compile
../trans-dev/configure
make
